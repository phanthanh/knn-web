const API_URL = process.env.REACT_APP_API_URL;

export const login = API_URL + '/tai-khoan/dang-nhap';
export const upload = API_URL + 'upload/image';
export const contractList = API_URL + '/ctv-bhyt/danh-sach';
export const contractDetail = API_URL + '/ctv-bhyt/tim-theo-phieu';
export const userInfo = API_URL + '/tai-khoan/danh-sach';
export const updateUserInfo = API_URL + '/tai-khoan/chinh-sua';
export const hospitalList = API_URL + '/danh-muc/benh-vien-bhyt';
export const provinceList = API_URL + '/danh-muc/thanh-pho';
export const districtList = API_URL + '/danh-muc/quan-huyen';
export const wardList = API_URL + '/danh-muc/phuong-xa';
export const healthInsuranceCreate = API_URL + '/ctv-bhyt/tao-moi';
export const healthInsuranceEdit = API_URL + '/ctv-bhyt/chinh-sua';
export const changePassword = API_URL + '/tai-khoan/doi-mat-khau';
export const searchCustomers = API_URL + '/danh-muc/tim-khach-hang';
export const uploadImage = API_URL + '/upload/image';
