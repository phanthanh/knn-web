const { REACT_APP_AUTH_USERNAME, REACT_APP_AUTH_PASSWORD } = process.env;

export const authUsername = REACT_APP_AUTH_USERNAME || '';
export const authPassword = REACT_APP_AUTH_PASSWORD || '';
