import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { Button, Col, Form, Input, Row } from 'antd';
import { login } from 'services/apis';
import { useAuth } from 'hooks/useAuth';
import Logo from 'assets/images/splash-logo.png';

const LoginPage: React.FC = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const { authenticated, isAuthenticated } = useAuth();
  const [loading, setLoading] = useState<boolean>(false);
  const [errorMessage, setErrorMessage] = useState<string>('');

  const from = location.state?.from?.pathname || '/';

  useEffect(() => {
    if (isAuthenticated) navigate('/');
  }, [isAuthenticated]);

  const onSubmit = (values: any) => {
    setLoading(true);
    login(values.username, values.password)
      .then((user) => {
        authenticated(user);
        navigate(from);
      })
      .catch((error) => setErrorMessage(error))
      .finally(() => {
        setLoading(false);
      });
  };

  return (
    <Row style={{ marginTop: 40 }}>
      <Col span={24} style={{ textAlign: 'center' }}>
        <img src={Logo} style={{ marginLeft: 50 }} />
      </Col>
      <Col span={12} offset={6} style={{ textAlign: 'center' }}>
        <Form
          name="basic"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          style={{ maxWidth: 600 }}
          initialValues={{ remember: true }}
          onFinish={onSubmit}
          autoComplete="off"
        >
          <Form.Item
            label="Tên đăng nhập"
            name="username"
            rules={[{ required: true, message: '' }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Mật khẩu"
            name="password"
            rules={[{ required: true, message: '' }]}
          >
            <Input.Password />
          </Form.Item>
          {errorMessage && (
            <Form.Item style={{ color: 'red' }}>{errorMessage}</Form.Item>
          )}
          <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
            <Button type="primary" htmlType="submit" loading={loading}>
              Đăng nhập
            </Button>
          </Form.Item>
        </Form>
      </Col>
    </Row>
  );
};

export default LoginPage;
