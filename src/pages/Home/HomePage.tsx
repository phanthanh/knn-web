import { Button } from 'antd';
import { useNavigate } from 'react-router-dom';
import { RoutePath } from '@constants';

const HomePage: React.FC = () => {
  const navigate = useNavigate();

  return (
    <Button onClick={() => navigate(RoutePath.contractCreate)}>
      Bảo hiểm y tế
    </Button>
  );
};

export default HomePage;
