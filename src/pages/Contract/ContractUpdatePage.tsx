import { notification, Tabs, TabsProps } from 'antd';
import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { RoutePath } from '@constants';

import { fetchContract } from 'services/apis';
import { ContractForm } from './components/ContractForm';
import { UploadForm } from './components/UploadForm';

const ContractUpdatePage: React.FC = () => {
  const { contractCode } = useParams();
  const navigate = useNavigate();
  const [contract, setContract] = useState<any>(null);
  const [images, setImages] = useState([]);

  const items: TabsProps['items'] = [
    {
      key: '1',
      label: `Thông tin khách hàng`,
      children: <ContractForm contract={contract} />,
    },
    {
      key: '2',
      label: `Giấy tờ tùy thân`,
      children: <UploadForm images={images} />,
    },
  ];

  useEffect(() => {
    if (contractCode) {
      fetchContract(contractCode)
        .then((data) => {
          setContract(data);
          setImages(data.HinhAnhs);
        })
        .catch((error) => {
          notification.error({ message: error.message });
          navigate(RoutePath.contractList);
        });
    }
  }, []);

  return <Tabs defaultActiveKey="1" items={items} />;
};

export default ContractUpdatePage;
