import { Tabs, TabsProps } from 'antd';
import React from 'react';
import { ContractForm } from './components/ContractForm';

const ContractCreatePage: React.FC = () => {
  const items: TabsProps['items'] = [
    {
      key: '1',
      label: `Thông tin khách hàng`,
      children: <ContractForm />,
    },
  ];

  return (
    <>
      <Tabs defaultActiveKey="1" items={items} />
    </>
  );
};

export default ContractCreatePage;
