import { Button, Card, Form, Upload, UploadFile } from 'antd';
import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { UploadOutlined } from '@ant-design/icons';
import { imagesToFormValues } from 'utils';
import { uploadImage } from 'services/apis';
import { useAuth } from 'hooks/useAuth';

export const UploadForm: React.FC<any> = ({ images }): JSX.Element => {
  const [form] = Form.useForm();
  const { contractCode } = useParams();
  const { userInfo } = useAuth();
  const [CMNDMatTruoc, setCMNDMatTruoc] = useState<UploadFile[]>([]);
  const [CMNDMatSau, setCMNDMatSau] = useState<UploadFile[]>([]);
  const [TheBH, setTheBH] = useState<UploadFile[]>([]);
  const [HoKhauChuHo, setHoKhauChuHo] = useState<UploadFile[]>([]);
  const [HoKhauNguoiMuaBH, setHoKhauNguoiMuaBH] = useState<UploadFile[]>([]);
  const [ThongTinChuyenKhoan, setThongTinChuyenKhoan] = useState<UploadFile[]>(
    [],
  );

  useEffect(() => {
    if (images) {
      form.setFieldsValue(imagesToFormValues(images));
    }
    const imgs = imagesToFormValues(images);
    for (let field of Object.keys(imgs)) {
      if (imgs[field]) {
        setImageValue(field, imgs[field]);
      }
    }
  }, [images]);

  const setImageValue = (fieldName: string, value?: string) => {
    if (fieldName === 'CMNDMatTruoc') {
      setCMNDMatTruoc([
        {
          uid: 'CMNDMatTruoc',
          name: '',
          url: value,
        },
      ]);
    } else if (fieldName === 'CMNDMatSau') {
      setCMNDMatSau([
        {
          uid: 'CMNDMatSau',
          name: '',
          url: value,
        },
      ]);
    } else if (fieldName === 'TheBH') {
      setTheBH([
        {
          uid: 'TheBH',
          name: '',
          url: value,
        },
      ]);
    } else if (fieldName === 'HoKhauChuHo') {
      setHoKhauChuHo([
        {
          uid: 'HoKhauChuHo',
          name: '',
          url: value,
        },
      ]);
    } else if (fieldName === 'HoKhauNguoiMuaBH') {
      setHoKhauNguoiMuaBH([
        {
          uid: 'HoKhauNguoiMuaBH',
          name: '',
          url: value,
        },
      ]);
    } else if (fieldName === 'ThongTinChuyenKhoan') {
      setThongTinChuyenKhoan([
        {
          uid: 'ThongTinChuyenKhoan',
          name: '',
          url: value,
        },
      ]);
    }
  };

  const handleUpload = async (options: any, fieldName: string) => {
    const { onSuccess, onError, file } = options;
    try {
      const res = await uploadImage(
        {
          ma: contractCode,
          loaiFile: fieldName,
          userID: userInfo?.UserID,
        },
        file,
      );
      onSuccess('Ok');
      setImageValue(fieldName, res);
    } catch (err) {
      console.log('Eroor: ', err);
      onError({ err });
    }
  };

  const handleChange = (value: any) => {
    if (value.file && value.file.status === 'removed') {
      const uid = value.file.uid;
      form.setFieldValue(uid, '');
      if (uid === 'CMNDMatTruoc') {
        setCMNDMatTruoc([]);
      } else if (uid === 'CMNDMatSau') {
        setCMNDMatSau([]);
      } else if (uid === 'TheBH') {
        setTheBH([]);
      } else if (uid === 'HoKhauChuHo') {
        setHoKhauChuHo([]);
      } else if (uid === 'HoKhauNguoiMuaBH') {
        setHoKhauNguoiMuaBH([]);
      } else if (uid === 'ThongTinChuyenKhoan') {
        setThongTinChuyenKhoan([]);
      }
    }
  };

  return (
    <Card>
      <Form
        form={form}
        name="basic"
        layout="inline"
        style={{ maxWidth: 800 }}
        autoComplete="off"
      >
        <Form.Item style={{ marginTop: 16 }} name="CMNDMatTruoc">
          <Upload
            accept="image/*"
            onChange={handleChange}
            customRequest={(v) => handleUpload(v, 'CMNDMatTruoc')}
            listType="picture"
            maxCount={1}
            fileList={CMNDMatTruoc}
            className="upload-list-inline"
          >
            <Button icon={<UploadOutlined />}>Ảnh CMND mặt trước</Button>
          </Upload>
        </Form.Item>
        <Form.Item style={{ marginTop: 16 }} name="CMNDMatSau">
          <Upload
            accept="image/*"
            onChange={handleChange}
            customRequest={(v) => handleUpload(v, 'CMNDMatSau')}
            listType="picture"
            maxCount={1}
            fileList={CMNDMatSau}
            className="upload-list-inline"
          >
            <Button icon={<UploadOutlined />}>Ảnh CMND mặt sau</Button>
          </Upload>
        </Form.Item>
        <Form.Item style={{ marginTop: 16 }} name="TheBH">
          <Upload
            accept="image/*"
            onChange={handleChange}
            customRequest={(v) => handleUpload(v, 'TheBH')}
            listType="picture"
            maxCount={1}
            fileList={TheBH}
            className="upload-list-inline"
          >
            <Button icon={<UploadOutlined />}>Thẻ bảo hiểm y tế cũ</Button>
          </Upload>
        </Form.Item>
        <Form.Item style={{ marginTop: 16 }} name="HoKhauChuHo">
          <Upload
            accept="image/*"
            onChange={handleChange}
            customRequest={(v) => handleUpload(v, 'HoKhauChuHo')}
            listType="picture"
            maxCount={1}
            fileList={HoKhauChuHo}
            className="upload-list-inline"
          >
            <Button icon={<UploadOutlined />}>Hộ khẩu chủ hộ</Button>
          </Upload>
        </Form.Item>
        <Form.Item style={{ marginTop: 16 }} name="HoKhauNguoiMuaBH">
          <Upload
            accept="image/*"
            onChange={handleChange}
            customRequest={(v) => handleUpload(v, 'HoKhauNguoiMuaBH')}
            listType="picture"
            maxCount={1}
            fileList={HoKhauNguoiMuaBH}
            className="upload-list-inline"
          >
            <Button icon={<UploadOutlined />}>
              Hộ khẩu người mua bảo hiểm
            </Button>
          </Upload>
        </Form.Item>
        <Form.Item style={{ marginTop: 16 }} name="ThongTinChuyenKhoan">
          <Upload
            accept="image/*"
            onChange={handleChange}
            customRequest={(v) => handleUpload(v, 'ThongTinChuyenKhoan')}
            listType="picture"
            maxCount={1}
            fileList={ThongTinChuyenKhoan}
            className="upload-list-inline"
          >
            <Button icon={<UploadOutlined />}>Thông tin chuyển khoản</Button>
          </Upload>
        </Form.Item>
      </Form>
    </Card>
  );
};
