import 'dayjs/locale/vi';
import locale from 'antd/es/date-picker/locale/vi_VN';
import {
  Button,
  Card,
  Checkbox,
  DatePicker,
  Form,
  Input,
  notification,
  Radio,
  Select,
} from 'antd';
import { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { Endpoint, RoutePath } from '@constants';
import { useAuth } from 'hooks/useAuth';
import { useFetch } from 'hooks/useFetch';
import {
  createHealthInsurance,
  fetchCustomerByKeyword,
  updateHealthInsurance,
} from 'services/apis';
import { Hospital, Zone } from '@types';
import {
  convertFormValuesToContract,
  jsonContractToFormValues,
  jsonCustomerToFormValues,
  validIdentityNumber,
} from 'utils';

var checkCustomerDebounce: any;

export const ContractForm: React.FC<any> = ({ contract }): JSX.Element => {
  const navigate = useNavigate();
  const [form] = Form.useForm();
  const { userInfo } = useAuth();

  const { contractCode } = useParams();
  const selectedProvince = Form.useWatch('provinceCode', form);
  const selectedOtherProvince = Form.useWatch('otherProvinceCode', form);
  const selectedDistrict = Form.useWatch('districtCode', form);
  const [otherProvince, setOtherProvince] = useState<boolean>(false);
  const { data: provinces } = useFetch<Zone[]>(Endpoint.provinceList);
  const { data: districts } = useFetch<Zone[]>(
    selectedProvince
      ? `${Endpoint.districtList}?MaThanhPho=${selectedProvince}`
      : '',
  );
  const { data: wards } = useFetch<Zone[]>(
    selectedDistrict
      ? `${Endpoint.wardList}?MaQuanHuyen=${selectedDistrict}`
      : '',
  );
  const { data: hopitals } = useFetch<Hospital[]>(
    (otherProvince ? selectedOtherProvince : selectedProvince)
      ? `${Endpoint.hospitalList}?MaThanhPho=${
          otherProvince ? selectedOtherProvince : selectedProvince
        }`
      : '',
  );

  useEffect(() => {
    if (contract) {
      form.setFieldsValue(jsonContractToFormValues(contract));
    }
    return () => {
      if (checkCustomerDebounce) {
        clearTimeout(checkCustomerDebounce);
      }
    };
  }, [contract]);

  const onSubmit = (values: { [key: string]: string }) => {
    const data = convertFormValuesToContract({
      ...values,
      UserID: userInfo?.UserID || '',
    });
    if (contractCode) {
      updateHealthInsurance(data)
        .then((code: string) => {
          notification.success({
            message: 'Gửi yêu cầu thành công. Vui lòng bổ sung thông tin',
          });
          navigate(`${RoutePath.contractCreate}/${code}`);
        })
        .catch((error: any) => notification.error({ message: error.message }));
    } else {
      createHealthInsurance(data)
        .then((code: string) => {
          notification.success({
            message: 'Gửi yêu cầu thành công. Vui lòng bổ sung thông tin',
          });
          navigate(`${RoutePath.contractCreate}/${code}`);
        })
        .catch((error: any) => notification.error({ message: error.message }));
    }
  };

  const handleChangeIdentity = (e: any) => {
    if (checkCustomerDebounce) clearTimeout(checkCustomerDebounce);
    const value = e.target.value;
    if (validIdentityNumber(value)) {
      fetchCustomerByKeyword(value).then((customer?: object) => {
        if (customer) {
          form.setFieldsValue(jsonCustomerToFormValues(customer));
        }
      });
    }
  };

  const handleChangeMasterData = (fieldName: string) => {
    if (fieldName === 'provinceCode') {
      form.setFieldValue('districtCode', '');
      if (!otherProvince) {
        form.setFieldValue('hospitalCode', '');
      }
    } else if (fieldName == 'districtCode') {
      form.setFieldValue('wardCode', '');
    } else if (fieldName == 'otherProvinceCode' && otherProvince) {
      form.setFieldValue('hospitalCode', '');
    }
  };

  return (
    <Card>
      <Form
        form={form}
        name="basic"
        labelCol={{ span: 10 }}
        wrapperCol={{ span: 14 }}
        style={{ maxWidth: 800 }}
        initialValues={{ duration: '12' }}
        onFinish={onSubmit}
        autoComplete="off"
      >
        <Input type="hidden" name="code" />
        <Form.Item
          label="Số chứng minh thư/căn cước công dân"
          name="identityNumber"
          rules={[{ required: true, message: 'Không được bỏ trống' }]}
        >
          <Input onChange={handleChangeIdentity} />
        </Form.Item>
        <Input type="hidden" name="customerCode" />
        <Form.Item
          label="Tên khách hàng"
          name="fullname"
          rules={[{ required: true, message: 'Không được bỏ trống' }]}
        >
          <Input />
        </Form.Item>
        <Form.Item label="Email" name="email">
          <Input type="email" />
        </Form.Item>
        <Form.Item label="Số điện thoại" name="phone">
          <Input type="phone" />
        </Form.Item>
        <Form.Item label="Số sổ bảo hiểm y tế" name="healthNumber">
          <Input />
        </Form.Item>
        <Form.Item label="Số sổ bảo hiểm xã hội" name="socialNumber">
          <Input />
        </Form.Item>
        <Form.Item
          name="gender"
          label="Giới tính"
          rules={[{ required: true, message: 'Không được bỏ trống' }]}
        >
          <Radio.Group>
            <Radio value="nam"> Nam </Radio>
            <Radio value="nu"> Nữ </Radio>
          </Radio.Group>
        </Form.Item>
        <Form.Item
          name="birthday"
          label="Ngày sinh"
          rules={[{ required: true, message: 'Không được bỏ trống' }]}
        >
          <DatePicker locale={locale} format={'YYYY-MM-DD'} />
        </Form.Item>
        <Form.Item
          label="Thành phố/tỉnh địa chỉ tạm trú"
          name="provinceCode"
          rules={[{ required: true, message: 'Không được bỏ trống' }]}
        >
          <Select
            options={(provinces || []).map((p: Zone) => ({
              value: p.Ma,
              label: p.Ten,
            }))}
            onChange={() => handleChangeMasterData('provinceCode')}
          />
        </Form.Item>
        <Form.Item
          label="Quận/huyện địa chỉ tạm trú"
          name="districtCode"
          rules={[{ required: true, message: 'Không được bỏ trống' }]}
        >
          <Select
            options={(districts || []).map((d: Zone) => ({
              value: d.Ma,
              label: d.Ten,
            }))}
            onChange={() => handleChangeMasterData('districtCode')}
          />
        </Form.Item>
        <Form.Item
          name="wardCode"
          label="Phường/xã địa chỉ tạm trú"
          rules={[{ required: true, message: 'Không được bỏ trống' }]}
        >
          <Select
            options={(wards || []).map((w: Zone) => ({
              value: w.Ma,
              label: w.Ten,
            }))}
          />
        </Form.Item>
        <Form.Item
          label="Nhập số nhà, đường phố"
          name="address"
          rules={[{ required: true, message: 'Không được bỏ trống' }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="otherProvince"
          label="Bệnh viện không thuộc tỉnh tạm trú?"
        >
          <Checkbox
            checked={otherProvince}
            onChange={() => setOtherProvince(!otherProvince)}
          />
        </Form.Item>
        {otherProvince && (
          <Form.Item
            label="Chọn Tỉnh/Thành phố đăng ký bảo hiểm"
            name="otherProvinceCode"
            rules={[{ required: true, message: 'Không được bỏ trống' }]}
          >
            <Select
              options={(provinces || []).map((p: Zone) => ({
                value: p.Ma,
                label: p.Ten,
              }))}
              onChange={() => handleChangeMasterData('otherProvinceCode')}
            />
          </Form.Item>
        )}
        <Form.Item
          label="Bệnh viện"
          name="hospitalCode"
          rules={[{ required: true, message: 'Không được bỏ trống' }]}
        >
          <Select
            options={(hopitals || []).map((h: Hospital) => ({
              value: h.Ma,
              label: h.Ten,
            }))}
          />
        </Form.Item>
        <Form.Item
          label="Số tháng bảo hiểm"
          name="duration"
          rules={[{ required: true, message: 'Không được bỏ trống' }]}
        >
          <Select>
            <Select.Option value="3">3 tháng</Select.Option>
            <Select.Option value="6">6 tháng</Select.Option>
            <Select.Option value="9">9 tháng</Select.Option>
            <Select.Option value="12">12 tháng</Select.Option>
          </Select>
        </Form.Item>
        <Form.Item label="Ghi chú" name="note">
          <Input.TextArea />
        </Form.Item>
        <Form.Item wrapperCol={{ offset: 10, span: 14 }}>
          <Button type="primary" htmlType="submit">
            {contractCode ? 'Cập nhật thông tin' : 'Gửi duyệt đơn bảo hiểm'}
          </Button>
        </Form.Item>
      </Form>
    </Card>
  );
};
