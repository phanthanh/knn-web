import moment from 'moment';
import React from 'react';
import { Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { useFetch } from 'hooks/useFetch';
import { useAuth } from 'hooks/useAuth';
import { Endpoint } from '@constants';
import { encodeQueryData } from 'utils';
import { Contract } from 'types';

const ContractListPage: React.FC = () => {
  const columns: ColumnsType<Contract> = [
    {
      title: 'Mã đơn',
      dataIndex: 'Ma',
      key: 'Ma',
    },
    {
      title: 'Tên khách hàng',
      dataIndex: 'TenKhachHang',
      key: 'TenKhachHang',
      width: '20%',
    },
    {
      title: 'CMND/CCCD',
      dataIndex: 'CMND',
      key: 'CMND',
    },
    {
      title: 'Số BHYT',
      dataIndex: 'SoBHYT',
      key: 'SoBHYT',
    },
    {
      title: 'Thời hạn',
      dataIndex: 'ThoiHan',
      key: 'ThoiHan',
    },
    {
      title: 'Trạng thái',
      dataIndex: 'TrangThai',
      key: 'TrangThai',
    },
  ];

  const { userInfo } = useAuth();

  const { data } = useFetch<Contract[]>(
    `${Endpoint.contractList}?${encodeQueryData({
      maTrangThai: '',
      userID: userInfo?.UserID,
      isCTV: userInfo?.IsCTV,
      tuNgay: '01-01-2022 00:00:00',
      denNgay: moment().format('DD-MM-YYYY HH:mm:ss'),
    })}`,
  );

  return <Table columns={columns} dataSource={data} />;
};

export default ContractListPage;
