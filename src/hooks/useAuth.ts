import { useEffect, useState } from 'react';
import { User } from '../types';

export const useAuth = () => {
  const [isAuthenticated, setIsAuthenticated] = useState<Boolean>(
    !!localStorage.getItem('auth'),
  );
  const [userInfo, setUserInfo] = useState<User | null>(null);

  useEffect(() => {
    const user = localStorage.getItem('auth');
    if (user) {
      setUserInfo(JSON.parse(user));
      setIsAuthenticated(true);
    }
  }, []);

  const authenticated = (userData: User) => {
    localStorage.setItem('auth', JSON.stringify(userData));
    setUserInfo(userData);
    setIsAuthenticated(true);
  };

  const unAuthenticate = () => {
    localStorage.removeItem('auth');
    setUserInfo(null);
    setIsAuthenticated(false);
  };

  return { isAuthenticated, userInfo, authenticated, unAuthenticate };
};
