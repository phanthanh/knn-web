import { RouteObject } from 'react-router-dom';
import { lazy, Suspense } from 'react';

import AuthLayout from './layout/AppLayout';
import { RoutePath } from './constants';
import ContractUpdatePage from './pages/Contract/ContractUpdatePage';

const Loadable = (Component: any) => (props: JSX.IntrinsicAttributes) =>
  (
    <Suspense fallback={<div />}>
      <Component {...props} />
    </Suspense>
  );
Loadable.displayName = Loadable;

const HomePage = Loadable(lazy(() => import('./pages/Home/HomePage')));
const LoginPage = Loadable(lazy(() => import('./pages/Login/LoginPage')));
const ContractListPage = Loadable(
  lazy(() => import('./pages/Contract/ContractListPage')),
);
const ContractCreatePage = Loadable(
  lazy(() => import('./pages/Contract/ContractCreatePage')),
);

export const routes: RouteObject[] = [
  {
    path: RoutePath.login,
    element: <LoginPage />,
  },
  {
    path: RoutePath.home,
    element: (
      <AuthLayout>
        <HomePage />
      </AuthLayout>
    ),
  },
  {
    path: RoutePath.contractList,
    element: (
      <AuthLayout>
        <ContractListPage />
      </AuthLayout>
    ),
  },
  {
    path: RoutePath.contractCreate,
    element: (
      <AuthLayout>
        <ContractCreatePage />
      </AuthLayout>
    ),
  },
  {
    path: `${RoutePath.contractCreate}/:contractCode`,
    element: (
      <AuthLayout>
        <ContractUpdatePage />
      </AuthLayout>
    ),
  },
];
