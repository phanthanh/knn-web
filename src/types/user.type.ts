export interface User {
  UserID: string;
  Ten: string;
  IsCTV: boolean;
  MaNhanVien: string;
}
