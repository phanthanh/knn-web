export interface Contract {
  Ma: string;
  TenKhachHang: string;
  CMND: string;
  SoBHYT: string;
  NgaySinh: string;
  DiaChi: string;
  BenhVien: string;
  ThoiHan: number;
  MaTrangThai: string;
  TrangThai: string;
  NhanVien: string;
}
