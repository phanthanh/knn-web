import axios from 'axios';

import { Config } from '../constants';

axios.interceptors.request.use(
  (request) => {
    request.auth = {
      username: Config.authUsername,
      password: Config.authPassword,
    };
    return request;
  },
  (error) => {
    console.log(error);
    return Promise.reject(error);
  },
);
