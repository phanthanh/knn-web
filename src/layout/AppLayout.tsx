import { Col, Layout, Row } from 'antd';
import { ReactElement } from 'react';
import { Navigate, useLocation } from 'react-router-dom';
import { LogoutOutlined } from '@ant-design/icons';
import Logo from '@assets/images/logo.svg';
import { useAuth } from 'hooks/useAuth';
import { RoutePath } from '@constants';
import Sidebar from './Sidebar';

const { Content, Header } = Layout;

function AuthLayout({ children }: { children: ReactElement | null }) {
  const { isAuthenticated, userInfo, unAuthenticate } = useAuth();
  const location = useLocation();

  return isAuthenticated ? (
    <Layout>
      <Header>
        <Row align="middle" justify="space-between" style={{ height: '100%' }}>
          <Col span={4} style={{ display: 'inline-flex' }}>
            <img src={Logo} height={42} />
          </Col>
          <Col>
            <span style={{ color: 'white' }}>Chào, {userInfo?.Ten} </span>
            <LogoutOutlined
              onClick={unAuthenticate}
              style={{ color: 'white' }}
            />
          </Col>
        </Row>
      </Header>
      <Layout>
        <Sidebar />
        <Content>{children}</Content>
      </Layout>
    </Layout>
  ) : (
    <Navigate to={RoutePath.login} replace state={{ from: location }} />
  );
}

export default AuthLayout;
