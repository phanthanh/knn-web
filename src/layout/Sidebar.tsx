import React from 'react';
import { Layout, Menu } from 'antd';
import { Link } from 'react-router-dom';
import { RoutePath } from '../constants';

const { Sider } = Layout;

const menuItems = [
  {
    path: RoutePath.home,
    title: 'Trang chủ',
  },
  {
    path: RoutePath.contractList,
    title: 'Danh sách đơn',
  },
  {
    path: RoutePath.contractCreate,
    title: 'Cấp đơn BHYT',
  },
];

const Sidebar: React.FC = () => {
  const items = menuItems.map((item) => ({
    key: item.path,
    label: <Link to={item.path}>{item.title}</Link>,
  }));

  return (
    <Sider width={300}>
      <Menu
        mode="inline"
        items={items}
        style={{ height: '100%', borderRight: 0 }}
      />
    </Sider>
  );
};

export default Sidebar;
