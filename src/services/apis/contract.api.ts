import axios from 'axios';
import { Endpoint } from '@constants';

export const createHealthInsurance = async (data: Object) => {
  try {
    const res = await axios.post(Endpoint.healthInsuranceCreate, data);
    if (res && res.data && res.data['Result'] === 'OK') {
      return res.data['SoPhieu'];
    }
    throw new Error(res.data['Message']);
  } catch (e) {
    throw (e as Error).message;
  }
};

export const updateHealthInsurance = async (data: Object) => {
  try {
    const res = await axios.post(Endpoint.healthInsuranceEdit, data);
    if (res && res.data && res.data['Result'] === 'OK') {
      return res.data['SoPhieu'];
    }
    throw new Error(res.data['Message']);
  } catch (e) {
    throw (e as Error).message;
  }
};

export const fetchContracts = async (params: Object) => {
  try {
    const res = await axios.get(Endpoint.contractList, { params });
    if (res && res.data && res.data['Result'] === 'OK') {
      return res.data['Data'];
    }
    throw new Error(res.data['Message']);
  } catch (e) {
    throw (e as Error).message;
  }
};

export const fetchContract = async (contractCode: String) => {
  try {
    const res = await axios.get(Endpoint.contractDetail, {
      params: { SoPhieu: contractCode },
    });
    if (res && res.data && res.data['Result'] === 'OK') {
      return res.data['Data'];
    }
    throw new Error(res.data['Message']);
  } catch (e) {
    throw (e as Error).message;
  }
};

export const uploadImage = async (params: Object, file: any) => {
  try {
    const formData = new FormData();
    formData.append('', file);
    const res = await axios.post(Endpoint.uploadImage, formData, {
      params,
      headers: { 'content-type': 'multipart/form-data' },
    });
    if (res && res.data && res.data['Result'] === 'OK') {
      return res.data['URL'];
    }
    throw new Error(res.data['Message']);
  } catch (e) {
    throw (e as Error).message;
  }
};
