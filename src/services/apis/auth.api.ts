import axios from 'axios';
import { Endpoint } from '@constants';

export const login = async (username: String, password: String) => {
  try {
    const res = await axios.post(Endpoint.login, {
      UserId: username,
      Password: password,
    });
    if (res && res.data && res.data['Result'] === 'OK') {
      const user = res.data;
      delete user['Result'];
      delete user['Message'];
      return user;
    }
    throw new Error(res.data['Message']);
  } catch (e) {
    throw (e as Error).message;
  }
};
