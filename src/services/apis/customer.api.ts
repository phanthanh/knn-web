import axios from 'axios';
import { Endpoint } from '@constants';

export const fetchCustomerByKeyword = async (keyword: String) => {
  try {
    const res = await axios.get(Endpoint.searchCustomers, {
      params: {
        dieukien: keyword,
      },
    });
    if (res && res.data && res.data['Result'] === 'OK') {
      const listCustomer = res.data['Data'];
      return listCustomer.length ? listCustomer[0] : null;
    }
    throw new Error(res.data['Message']);
  } catch (e) {
    console.error(e);
    return null;
  }
};
