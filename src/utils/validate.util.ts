export const validIdentityNumber = (value?: string) => {
  if (value) {
    return /^(\d{9}|\d{12})$/.test(value);
  }
  return false;
};
