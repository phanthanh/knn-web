import moment from 'moment';

export const encodeQueryData = (data: { [key: string]: any }) => {
  const ret = [];
  for (let d of Object.keys(data)) {
    ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
  }
  return ret.join('&');
};

export const convertFormValuesToContract = (values: {
  [key: string]: string;
}) => {
  return {
    MaBenhVien: values.hospitalCode,
    SoThang: values.duration,
    GhiChu: values.note,
    KhachHang: {
      Ma: values.customerCode,
      Ten: values.fullname,
      DienThoai: values.phone,
      Email: values.email,
      SoBHXH: values.socialNumber,
      SoBHYT: values.healthNumber,
      CMND: values.identityNumber,
      MaGioiTinh: values.gender,
      NgaySinh: values.birthday
        ? moment(values.birthday).format('YYYY-MM-DD')
        : '',
      MaThanhPhoTamTru: values.provinceCode,
      MaQuanHuyenTamTru: values.districtCode,
      MaPhuongXaTamTru: values.wardCode,
      DiaChiTamTru: values.address,
    },
    UserID: values?.UserID,
  };
};

export const jsonCustomerToFormValues = (customer: { [key: string]: any }) => {
  return {
    customerCode: customer.Ma,
    identityNumber: customer.CMND,
    fullname: customer.Ten,
    email: customer.Email,
    phone: customer.DienThoai,
    socialNumber: customer.SoBHXH,
    healthNumber: customer.SoBHYT,
    gender: customer.MaGioiTinh,
    birthday: customer.NgaySinh ? moment(customer.NgaySinh) : '',
    provinceCode: customer.MaThanhPhoTamTru,
    districtCode: customer.MaQuanHuyenTamTru,
    wardCode: customer.MaPhuongXaTamTru,
    address: customer.DiaChiTamTru,
  };
};

export const jsonContractToFormValues = (jsonContract: {
  [key: string]: any;
}) => {
  return {
    ...jsonCustomerToFormValues(jsonContract.KhachHang),
    hospitalCode: jsonContract.MaBenhVien,
    duration: jsonContract.SoThang,
    note: jsonContract.GhiChu,
  };
};

export const imagesToFormValues = (
  images: { LoaiFile: string; URL: string }[],
) => {
  const result = {} as { [key: string]: any };
  if (images.length) {
    for (let image of images) result[image.LoaiFile] = image.URL;
  }
  return result;
};
