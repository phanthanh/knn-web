import moment from 'moment';

export const formatDatetime = (
  value: string,
  formatInput: string,
  formatOutput: string,
) => {
  return moment(value, formatOutput, true).format(formatInput);
};
