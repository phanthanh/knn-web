const CracoAliasPlugin = require('craco-alias');
const path = require('path');

module.exports = {
  webpack: {
    alias: {
      '@': path.resolve(__dirname, 'src'),
    },
  },
  style: {
    postOptions: {
    },
  },
  plugins: [
    {
      plugin: CracoAliasPlugin,
      options: {
        source: 'tsconfig',
        baseUrl: './src',
        tsConfigPath: 'tsconfig.json',
      },
    },
  ],
  eslint: {
    enable: true,
    mode: 'extends' || 'file',
    configure: {},
    configure: (eslintConfig, { env, paths }) => {
      return eslintConfig;
    },
    pluginOptions: {},
    pluginOptions: (eslintPluginOptions, { env, paths }) => {
      return eslintPluginOptions;
    },
  },
};