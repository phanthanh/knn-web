FROM node:14-alpine as dependencies
ENV NODE_ENV production
WORKDIR /app
COPY package.json ./
RUN yarn install
COPY . .
RUN yarn build

FROM hub.vela.com/public/httpd:2.4
EXPOSE 80
COPY --from=dependencies /app/build /usr/local/apache2/htdocs/