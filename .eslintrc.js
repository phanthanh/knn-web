module.exports = {
  root: true,
  env: {
    node: true,
    es6: true,
  },
  parserOptions: { ecmaVersion: 8, sourceType: 'module' },
  ignorePatterns: ['node_modules/*'],
  extends: ['eslint:recommended'],
  overrides: [
    {
      files: ['**/*.ts', '**/*.tsx'],
      settings: {
        react: { version: 'detect' },
        'import/resolver': {
          "node": {
            "moduleDirectory": ["node_modules", "src/"]
          }
        },
      },
      env: {
        browser: true,
        node: true,
        es6: true,
      },
      extends: [
        "plugin:react/recommended",
        "eslint-config-prettier"
      ],
      plugins: [
        "prettier",
        "import"
      ],
      rules: {
        'no-restricted-imports': [
          'error',
          {
            patterns: ['@/pages/*/*'],
          },
        ],
        'linebreak-style': ['error', 'unix'],
        'react/prop-types': 'off',

        // 'import/order': [
        //   'error',
        //   {
        //     groups: ['builtin', 'external', 'internal', 'parent', 'sibling', 'index', 'object'],
        //     'newlines-between': 'always',
        //     alphabetize: { order: 'asc', caseInsensitive: true },
        //   },
        // ],
        'import/default': 'off',
        'import/no-named-as-default-member': 'off',
        'import/no-named-as-default': 'off',
        'react/react-in-jsx-scope': 'off',
        'react/display-name': 'off',
        'jsx-a11y/anchor-is-valid': 'off',
        'no-unused-vars': 'off',
        'explicit-function-return-type': ['off'],
        'explicit-module-boundary-types': ['off'],
        'no-empty-function': ['off'],
        'no-explicit-any': ['off'],
        'quotes': [2, "single", { "avoidEscape": true, "allowTemplateLiterals": true }],
        'prettier/prettier': 0,
        'no-undef': 'off'
      },
    },
  ],
};